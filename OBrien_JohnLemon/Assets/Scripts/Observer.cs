﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    // intiate your variables
    public Transform player;
    public GameEnding gameEnding;

    bool m_IsPlayerInRange;

    private void OnTriggerEnter(Collider other)
    {
        // if the object in the collider is the player
        if (other.transform == player)
        {
            // set the in range bool to true
            m_IsPlayerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // if the player exits the range
        if (other.transform == player) ;
        {
            // set the in range bool to false
            m_IsPlayerInRange = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_IsPlayerInRange)
        {
            // initiate movement and direction for the player
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            // if the player can be seen
            if (Physics.Raycast(ray, out raycastHit))
            {
                // and the player is in the collider
                if (raycastHit.collider.transform == player)
                {
                    // initate the caught player function
                    gameEnding.CaughtPlayer ();
                }
            }
        }
    }
}
