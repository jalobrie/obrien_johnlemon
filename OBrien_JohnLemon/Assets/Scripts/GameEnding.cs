﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    //intiate your public variables
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;

    // intitiate other variables
    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;

    private void OnTriggerEnter(Collider other)
    {
        // if the object colliding with the object is the player
        if (other.gameObject == player)
        {
            // player exit bool is set to true
            m_IsPlayerAtExit = true;
        }
    }

    public void CaughtPlayer ()
    {
        // caught player bool is true
        m_IsPlayerCaught = true;
    }

    public void WinGame()
    {
        // player wins when the player at exit bool is set to true
        m_IsPlayerAtExit = true;
    }



    // Update is called once per frame
    void Update()
    {
        // if the player exit bool is true
        if(m_IsPlayerAtExit)
        {
            // present exit image
            EndLevel (exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        // if player caught is true
        else if (m_IsPlayerCaught)
        {
            // present caught image
            EndLevel (caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        // if audio played bool is true
        if(!m_HasAudioPlayed)
        {
            // play the audio
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        // set a timer between when an action occurs and when the game ends
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            // if you want to restart
            if(doRestart)
            {
                // restart the game
                SceneManager.LoadScene(0);
            }
            else
            {
                // or else end the game
                Application.Quit();
            }
        }
    }
}
