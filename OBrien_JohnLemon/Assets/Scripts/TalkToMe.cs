﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkToMe : MonoBehaviour
{
    public Text textBox;

    public string toSay;

    public string endSay;

    public GameEnding gameEnding;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textBox.gameObject.SetActive(true);
            textBox.text = toSay;
            PlayerMovement pm = other.gameObject.GetComponent<PlayerMovement>();
            if (pm.hasCube && pm.hasSphere && pm.hasCylinder) // collected all the objects and end the game
            {
                textBox.text = endSay;
                Invoke ("WinGame", 3f);
            }
        }
    }


    void WinGame()
    {
        gameEnding.WinGame();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textBox.gameObject.SetActive(false);
        }
    }
}
