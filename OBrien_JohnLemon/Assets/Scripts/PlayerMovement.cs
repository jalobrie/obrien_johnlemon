﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // initiate your variables
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    public bool hasCube;
    public bool hasSphere;
    public bool hasCylinder;

    // Start is called before the first frame update
    void Start()
    {
        // give the john lemon object an animation, rigid body, and audio
      m_Animator = GetComponent<Animator> ();
      m_Rigidbody = GetComponent<Rigidbody> ();
      m_AudioSource = GetComponent<AudioSource>();      
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // give the john lemon object the ability to move 
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize ();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool ("IsWalking", isWalking);

        // if the player is walking 
        if(isWalking)
        {
            // and if there is audio in the game
            if(!m_AudioSource.isPlaying)
            {
                // cue the walking audio for the player movement
                m_AudioSource.Play();
            }
        }
        else
        {
            // or stop the movement audio
            m_AudioSource.Stop();
        }

        // allow the player to rotate and designate a speed
        Vector3 desiredForward = Vector3.RotateTowards (transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation (desiredForward);
    }

    void OnAnimatorMove()
    {
        // cue a movement aniamtion speed
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Shrink"))
        {
            transform.localScale = Vector3.one * .5f;
            Invoke("ReturnToFullSize", 5f);

            Destroy(other.gameObject);
        }

        // if the player picks up a cube tag
        if (other.gameObject.CompareTag("PickUpCube"))
        {
            // make that object a child of the player
            other.transform.parent = transform;
            hasCube = true;
        }
        // if the player picks up a sphere tag
        if (other.gameObject.CompareTag("PickUpSphere"))
        {
            // make that object a child of the player
            other.transform.parent = transform;
            hasSphere = true;
        }
        // if the player picks up a cylinder tag
        if (other.gameObject.CompareTag("PickUpCylinder"))
        {
            // make that object a child of the player
            other.transform.parent = transform;
            hasCylinder = true;
        }
}

    void ReturnToFullSize()
    {
        transform.localScale = Vector3.one;
    }
    /*
    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            transform.DetachChildren
        }
        
            
    }
    */
}
